# Déploiement de l'infrastructure OPSCI : 
Ce dépôt contient les fichiers et les instructions nécessaires pour déployer la première partie de l'infrastructure de OPSCI. Ce projet est réalisé dans le cadre de notre cours universitaire et consiste à déployer un serveur Strapi, une base de données PostgreSQL et un frontend web utilisant l'API de Strapi.

# Objectif du projet :
L'objectif principal de ce projet est de déployer les composants nécessaires pour faire fonctionner l'application OPSCI, en l'occurrence Strapi, sa base de données et le frontend web.

# Livrables : 
## Ce projet comporte les livrables suivants :

-Vidéo de démonstration : Une vidéo screencast de 6 minutes présentant les fonctionnalités de l'application déployée.

-Dépôt Git : Un dépôt Git accessible en ligne contenant tous les fichiers nécessaires à la mise en place de l'infrastructure, y compris les Dockerfiles, les scripts shell, etc. La vidéo de démonstration sera également incluse dans ce dépôt.

-Scripts de gestion : Des scripts permettant de lancer et d'arrêter les différents éléments de l'application.

-README.md : Ce fichier README.md qui fournit des informations sur le déploiement de l'application.

# Installation et déploiement : 

# Prérequis :
Docker doit être installé sur votre système.
Yarn doit être installé pour la configuration de Strapi.

# COMMENT ON A PROCÉDÉ : 

## Première approche :
Dans un premier temps, nous avons créé tous les conteneurs individuellement sans utiliser de fichier docker-compose.yml. Ensuite, nous les avons liés dans le réseau ad-network et avons procédé à la création des produits avec le gestionnaire de contenu de Strapi. Cependant, cette méthode nous obligeait à surveiller les logs du conteneur Strapi pour accéder à l'interface de gestion de Strapi. De plus, nous devions également exécuter le conteneur Frontend en parallèle pour y accéder.

## Deuxième approche :
Nous avons ensuite opté pour l'utilisation du fichier docker-compose.yml, ce qui nous a simplifié grandement la tâche. Grâce à celui-ci, nous pouvons exécuter deux commandes simples : **"docker compose build" et "docker compose run"**. Cela nous permet de gérer efficacement les différents conteneurs et leurs interactions. Cependant, nous n'avons pas adopté cette approche dès le début en raison de problèmes rencontrés avec notre fichier docker-compose.yml, qui ne fonctionnait pas parfaitement.
Pour des raisons de confidentialités, vous n'allez pas pouvoir avoir accès au .env.

Cette évolution dans notre méthode de déploiement nous a permis d'améliorer considérablement notre processus de développement et de déploiement de l'application OPSCI.



# Les commandes utilisés pour la première approche : 

## Créer le conteneur PostgreSQL
docker run -dit -p 5432:5432 -e POSTGRES_PASSWORD=safepassword -e POSTGRES_USER=strapi --name strapi-pg postgres

## Créer le réseau Docker ad-network
docker network create ad-network

## Créer le conteneur Strapi et le connecter au réseau ad-network
### Naviguer dans le répertoire du projet ad_opsci
cd ad_opsci

### Construire l'image Docker pour Strapi
docker build -t ad-proj-im .

### Démarrer le conteneur Strapi à partir de l'image ad-proj-im
docker run -d -p 1337:1337 ad-proj-im

## Cloner le dépôt du frontend web
git clone https://github.com/arthurescriou/opsci-strapi-frontend

## Construire l'image Docker du frontend
cd opsci-strapi-frontend
touch Dockerfile  

## Construire l'image Docker du frontend
docker build -t front-im .

## Créer et démarrer le conteneur Frontend et le connecter au réseau ad-network
docker run -d --network ad-network front-im

## Inspecter le réseau Docker ad-network
docker network inspect ad-network


# Les commandes pour Deuxième approche :
Veuillez vous référer à celle-ci lors de l'exécution.

-docker compose build
-docker compose up

## Le nom des containers et leurs images respectives : 
Container name : front-ad, image name : front-im-ad.
Container name : strapi, image name : strapi:latest.
Container name : strapiDB, image name : postgres:16.2-bullseye.
Network : Strapi.


## Ports utilisés : 
-Strapi : 
 ports:
      - "1336:1337"

-React : 
 ports : 
      - "5173:5173"


# Les commandes git
-git init
-git add .
-git commit -m "...".
-git push -u origin main


# Lien pour la vidéo 
**https://youtu.be/7ZEGqfbIxmE**


